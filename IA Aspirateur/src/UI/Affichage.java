package UI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import Elements.Environment;
import Elements.Environment.PassiveElement;

public class Affichage extends JFrame{

	private Environment environnement;
	
	private Image aspirateur;
	private Image poussiere;
	private Image bijou;
	private Image poussiereETbijou;
		
	public Affichage(Environment environnement) {
		super("Agent Aspirateur");
		getContentPane().setBackground(Color.WHITE);
        setSize(1000, 1000);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        
		this.environnement = environnement;
		
		try {
			this.aspirateur = ImageIO.read(new File("ressources\\aspirateur.png"));
			this.poussiere = ImageIO.read(new File("ressources\\poussiere.png"));
			this.bijou = ImageIO.read(new File("ressources\\bijou.png"));
			this.poussiereETbijou = ImageIO.read(new File("ressources\\poussiereETbijou.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void paint(Graphics g) {
       //super.paint(g);
        
        for(int i=100; i<900; i+=160) {
        	for(int j=100; j<900; j+=160) {
        		g.drawRect(i, j, 160, 160);
        	}
        }
        int indexi = 0;
        int indexj = 0;
        for(PassiveElement[] list : this.environnement.getMap()) {
        	for(PassiveElement e : list) {
            	if(e == PassiveElement.VACCUM) {
            		g.clearRect(100+indexi*160, 100+indexj*160, 160, 160);
            		g.drawImage(aspirateur, 100+indexi*160+10, 100+indexj*160+10 , 150, 150, null);
            	}
            	if(e == PassiveElement.DIRT) {
            		g.clearRect(100+indexi*160, 100+indexj*160, 160, 160);
            		g.drawImage(poussiere, 100+indexi*160+10, 100+indexj*160+10 , 150, 150, null);
            	}
            	if(e == PassiveElement.JEWEL) {
            		g.clearRect(100+indexi*160, 100+indexj*160, 160, 160);
            		g.drawImage(bijou, 100+indexi*160+10, 100+indexj*160+10 , 150, 150, null);
            	}
            	if(e == PassiveElement.DIRTJEWEL) {
            		g.clearRect(100+indexi*160, 100+indexj*160, 160, 160);
            		g.drawImage(poussiereETbijou, 100+indexi*160+10, 100+indexj*160+10 , 150, 150, null);
            	}
            	if(e == null) {
            		g.clearRect(100+indexi*160, 100+indexj*160, 160, 160);
            	}
            	g.drawRect(100+indexi*160, 100+indexj*160, 160, 160);
            	indexi+=1;
            }
        	indexi = 0;
        	indexj+=1;
        }
    }
}
