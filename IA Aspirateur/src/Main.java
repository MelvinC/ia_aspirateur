import Elements.Environment;
import Elements.VaccumInformed;
import Elements.VaccumUninformed;

public class Main {

	public static void main(String[] args) {
		Environment e = new Environment();
		Thread environment = new Thread(e);
		//VaccumUninformed v = new VaccumUninformed(e);
		VaccumInformed v = new VaccumInformed(e);
		Thread vaccum = new Thread(v);
		environment.start();
		vaccum.start();
	}

}
