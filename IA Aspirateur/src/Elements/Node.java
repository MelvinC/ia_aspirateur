package Elements;

import Elements.Environment.PassiveElement;

public class Node {
	private Node parent;
	private int xPos;
	private int yPos;
	private PassiveElement elem;
	private int depth;
	private int heuristic;

	public Node(int x, int y, PassiveElement elem) {
		this(null, x, y, elem, 0);
	}
	
	public Node(Node parent, int xPos, int yPos, PassiveElement elem, int depth) {
		this.parent = parent;
		this.xPos = xPos;
		this.yPos = yPos;
		this.elem = elem;
		this.depth = depth;
	}

	public PassiveElement getElement() {
		return elem;
	}

	public int getDepth() {
		return depth;
	}

	public int getX() {
		return xPos;
	}

	public int getY() {
		return yPos;
	}

	public Node getParent() {
		return parent;
	}

	public int getHeuristic() {
		return heuristic;
	}

	public void setHeuristic(int heuristic) {
		this.heuristic = heuristic;
	}
}
