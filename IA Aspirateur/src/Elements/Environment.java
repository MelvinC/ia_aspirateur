package Elements;

import UI.Affichage;

public class Environment implements Runnable, Cloneable {
	public enum PassiveElement {
		DIRT, JEWEL, DIRTJEWEL, VACCUM
	}

	private PassiveElement[][] map = new PassiveElement[5][5];
	
	private Affichage affichage;

	public Environment() {
		for (int i = 0; i < 5; i++)
			for (int j = 0; j < 5; j++)
				map[i][j] = null;
		generateDirt();
		this.affichage = new Affichage(this);
		this.affichage.setVisible(true);
	}

	@Override
	public void run() {
		while (true) {
			if (shouldHaveDirt()) {
				generateDirt();
			}
			if (shoudHaveJewel()) {
				generateJewel();
			}
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void print() {
		System.out.println("__________________________");
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.print("|" + map[i][j]);
			}
			System.out.println("|");
		}
		System.out.println("__________________________");
		this.affichage.repaint();
	}

	private boolean shouldHaveDirt() {
		double rand = Math.random() * 101;
		if (rand <= 1)
			return true;
		return false;
	}

	private boolean shoudHaveJewel() {
		double rand = Math.random() * 101;
		if (rand <= 0.1)
			return true;
		return false;
	}

	private void generateDirt() {
		int x = (int) (Math.random() * 5);
		int y = (int) (Math.random() * 5);
		PassiveElement square = map[x][y];
		if (square == PassiveElement.JEWEL)
			map[x][y] = PassiveElement.DIRTJEWEL;
		else if (square != PassiveElement.DIRT && square != PassiveElement.DIRTJEWEL)
			map[x][y] = PassiveElement.DIRT;
	}

	private void generateJewel() {
		int x = (int) (Math.random() * 5);
		int y = (int) (Math.random() * 5);
		PassiveElement square = map[x][y];
		if (square == PassiveElement.DIRT)
			map[x][y] = PassiveElement.DIRTJEWEL;
		else if (square != PassiveElement.JEWEL && square != PassiveElement.DIRTJEWEL)
			map[x][y] = PassiveElement.JEWEL;
	}

	public PassiveElement[][] getMap() {
		return this.map;
	}

	public Environment copy() {
		try {
			return (Environment) this.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
}
