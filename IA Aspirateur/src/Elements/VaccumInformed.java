package Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import Elements.Environment.PassiveElement;

public class VaccumInformed implements Runnable {

	private Environment environment;
	private int xPos = 0;
	private int yPos = 0;
	private double perf = 0;
	private File f = new File("perf.txt");
	private int compteur = 0;
	private int totalPerf = 0;

	public VaccumInformed(Environment environment) {
		this.environment = environment;
	}

	@Override
	public void run() {
		Node root = new Node(0, 0, PassiveElement.VACCUM);
		environment.getMap()[this.xPos][this.yPos] = PassiveElement.VACCUM;
		this.environment.print();
		while (true) {
			Environment envTemp = this.environment.copy();
			Node end = null;
			if (isNotEmpty(envTemp)) {
				envTemp.print();
				ArrayList<Node> elems = getElems(envTemp);
				ArrayList<Node> fringe = new ArrayList<Node>();
				root.setHeuristic(getHeuristic(root, elems));
				end = explo(root, envTemp, elems, fringe);
				moveToPurpose(end);
				root = new Node(end.getX(), end.getY(), PassiveElement.VACCUM);
			}
		}
	}
	
	public void runWithPerf() {
		Node root = new Node(0, 0, PassiveElement.VACCUM);
		environment.getMap()[this.xPos][this.yPos] = PassiveElement.VACCUM;
		this.environment.print();
		while (compteur<100) {
			Environment envTemp = this.environment.copy();
			Node end = null;
			if (isNotEmpty(envTemp)) {
				envTemp.print();
				ArrayList<Node> elems = getElems(envTemp);
				ArrayList<Node> fringe = new ArrayList<Node>();
				root.setHeuristic(getHeuristic(root, elems));
				end = explo(root, envTemp, elems, fringe);
				moveToPurpose(end);
				root = new Node(end.getX(), end.getY(), PassiveElement.VACCUM);
				compteur++;
			}
		}
		System.out.println(totalPerf/100);
	}

	private ArrayList<Node> getElems(Environment envTemp) {
		ArrayList<Node> elems = new ArrayList<Node>();
		for (int i = 0; i < 5; i++)
			for (int j = 0; j < 5; j++)
				if (envTemp.getMap()[i][j] != null && envTemp.getMap()[i][j] != PassiveElement.VACCUM)
					elems.add(new Node(i, j, envTemp.getMap()[i][j]));
		return elems;
	}

	private boolean isNotEmpty(Environment envTemp) {
		for (int i = 0; i < 5; i++)
			for (int j = 0; j < 5; j++)
				if (envTemp.getMap()[i][j] != null && envTemp.getMap()[i][j] != PassiveElement.VACCUM)
					return true;
		return false;
	}

	private void moveToPurpose(Node n) {
		ArrayList<Integer> xs = new ArrayList<Integer>();
		ArrayList<Integer> ys = new ArrayList<Integer>();
		if (n.getParent() != null) {
			while (n.getParent() != null) {
				xs.add(n.getX());
				ys.add(n.getY());
				n = n.getParent();
			}
			PassiveElement temp = null;
			while (xs.size() > 1) {
				environment.getMap()[this.xPos][this.yPos] = temp;
				this.xPos = xs.remove(xs.size() - 1);
				this.yPos = ys.remove(ys.size() - 1);
				this.perf--;
				temp = environment.getMap()[this.xPos][this.yPos];
				environment.getMap()[this.xPos][this.yPos] = PassiveElement.VACCUM;
				environment.print();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			environment.getMap()[this.xPos][this.yPos] = temp;
			System.out.println(xs.size());
			this.xPos = xs.remove(xs.size() - 1);
			this.yPos = ys.remove(ys.size() - 1);
			this.perf--;
		}
		if (environment.getMap()[this.xPos][this.yPos] == PassiveElement.JEWEL) {
			environment.getMap()[this.xPos][this.yPos] = null;
			System.out.println("The jewel has been picked up.");
			this.perf += 10;
		}
		if (environment.getMap()[this.xPos][this.yPos] == PassiveElement.DIRTJEWEL) {
			environment.getMap()[this.xPos][this.yPos] = PassiveElement.DIRT;
			System.out.println("The jewel has been picked up.");
			environment.print();
			this.perf += 10;
		}
		if (environment.getMap()[this.xPos][this.yPos] == PassiveElement.DIRT) {
			environment.getMap()[this.xPos][this.yPos] = null;
			System.out.println("The dirt has been vaccumed.");
			this.perf += 10;

		}
		this.totalPerf += this.perf;
		this.perf = 0;
		environment.getMap()[this.xPos][this.yPos] = PassiveElement.VACCUM;
		environment.print();
	}

	private Node explo(Node n, Environment envTemp, ArrayList<Node> elems, ArrayList<Node> fringe) {
		if (n.getHeuristic() - n.getDepth() == 0)
			return n;
		else {
			if (n.getX() < 4 && (n.getParent() == null
					|| (n.getParent().getX() != n.getX() + 1 || n.getParent().getY() != n.getY()))) {
				Node c = new Node(n, n.getX() + 1, n.getY(), envTemp.getMap()[n.getX() + 1][n.getY()],
						n.getDepth() + 1);
				c.setHeuristic(getHeuristic(c, elems));
				fringe.add(c);

			}
			if (n.getY() < 4 && (n.getParent() == null
					|| (n.getParent().getX() != n.getX() || n.getParent().getY() != n.getY() + 1))) {
				Node c = new Node(n, n.getX(), n.getY() + 1, envTemp.getMap()[n.getX()][n.getY() + 1],
						n.getDepth() + 1);
				c.setHeuristic(getHeuristic(c, elems));
				fringe.add(c);
			}
			if (n.getX() > 0 && (n.getParent() == null
					|| (n.getParent().getX() != n.getX() - 1 || n.getParent().getY() != n.getY()))) {
				Node c = new Node(n, n.getX() - 1, n.getY(), envTemp.getMap()[n.getX() - 1][n.getY()],
						n.getDepth() + 1);
				c.setHeuristic(getHeuristic(c, elems));
				fringe.add(c);
			}
			if (n.getY() > 0 && (n.getParent() == null
					|| (n.getParent().getX() != n.getX() || n.getParent().getY() != n.getY() - 1))) {
				Node c = new Node(n, n.getX(), n.getY() - 1, envTemp.getMap()[n.getX()][n.getY() - 1],
						n.getDepth() + 1);
				c.setHeuristic(getHeuristic(c, elems));
				fringe.add(c);
			}
			int iTemp = 10000;
			Node nTemp = null;
			for (Node child : fringe) {
				if (child.getHeuristic() < iTemp) {
					iTemp = child.getHeuristic();
					nTemp = child;
				}
			}
			fringe.remove(nTemp);
			return explo(nTemp, envTemp, elems, fringe);
		}

	}

	public int getHeuristic(Node n, ArrayList<Node> elems) {
		int g = n.getDepth();
		int h = 10000;
		for (Node elem : elems) {
			if (getDistance(n, elem) < h) {
				h = getDistance(n, elem);
			}
		}
		return h + g;
	}

	private int getDistance(Node n, Node elem) {
		return Math.abs(n.getX() - elem.getX()) + Math.abs(n.getY() - elem.getY());
	}
}
